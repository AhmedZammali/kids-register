<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $nh = Loader::helper('navigation'); ?>

<nav class="">
	<?php  foreach ($links as $link){ ?>
		<div class="col-sm-6">
			<a href="<?php  echo $link->url; ?>" title="<?php echo htmlentities($link->text, ENT_QUOTES, APP_CHARSET); ?>" class="btn btn-color-2 btn-lg btn-block">
				<?php echo htmlentities($link->text, ENT_QUOTES, APP_CHARSET); ?>
			</a>
		</div>
	<?php } ?>
</nav>
