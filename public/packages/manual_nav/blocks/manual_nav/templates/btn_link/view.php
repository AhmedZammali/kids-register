<?php foreach($links as $link): ?>
    <a href="<?php echo $link->url; ?>" class="btn btn-link" title="<?php echo $link->text; ?>"><?php echo $link->text; ?></a>
<?php endforeach; ?>