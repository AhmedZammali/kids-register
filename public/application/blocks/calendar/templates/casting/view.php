<?php defined('C5_EXECUTE') or die('Access Denied.');
$p=Page::getCurrentPage();
echo $p->getAttribute('center');

if (!isset($calendar) || !is_object($calendar)) {
    $calendar = null;
}

$c = Page::getCurrentPage();
if ($c->isEditMode()) {
    $loc = Localization::getInstance();
    $loc->pushActiveContext(Localization::CONTEXT_UI);
    ?><div class="ccm-edit-mode-disabled-item"><?=t('Calendar disabled in edit mode.')?></div><?php
    $loc->popActiveContext();
} elseif ($calendar !== null && $permissions->canViewCalendar()) { ?>
    <div class="ccm-block-calendar-wrapper" data-calendar="<?=$bID?>"></div>

    <script>
        $("#sel1").change(function() {
            
            $(".fc-day.fc-widget-content").css('background-color', 'white');
            $( ".list-time" ).remove();
            $( ".list-times" ).remove(); 
            var RegionName = $('#sel1').find(":selected").text();
            var RegionValue = $('#sel1').find(":selected").val();
            
            if(RegionValue==""){
                $('.fc-toolbar.fc-header-toolbar').hide();
                $('.fc-view-container').hide();
                $("input").prop('disabled', true);
                return;
            }
            $('.fc-toolbar.fc-header-toolbar').show();
            $('.fc-view-container').show();
            $('div[data-calendar=<?=$bID?>]').fullCalendar({
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: '<?= $viewTypeString ? $viewTypeString : ''; ?>'
                },
                locale: <?= json_encode(Localization::activeLanguage()); ?>,
                views: {
                    listDay: { buttonText: '<?= t('list day'); ?>' },
                    listWeek: { buttonText: '<?= t('list week'); ?>' },
                    listMonth: { buttonText: '<?= t('list month'); ?>' },
                    listYear: { buttonText: '<?= t('list year'); ?>' }
                },
                events: '<?=$view->action('get_events')?>',
                eventAfterAllRender: function() {
                    $(".fc-day.fc-widget-content").css('background-color', 'white');
                    $('a.fc-day-grid-event.fc-h-event.fc-event.fc-start.fc-end').css({"border": "1px solid rgb(233, 30, 99)"});
                },

                dayClick: function(date, allDay, jsEvent, view) {
                    
                    $('.hidden_name').val("");
                    $('.hidden_date').val("");
                    $('.hidden_id').val("");
                    $('#confirmer_cast').prop('disabled', true);
                    RegionName = $('#sel1').find(":selected").text();
                    $(".fc-day.fc-widget-content").css('background-color', 'white');
                    var daycliked = $(this);
                    var date_fin = new Date(date.format());
                    date_fin.setDate(date_fin.getDate() + 1);
                    date_fin=date_fin.toISOString();
                    $.getJSON({
                        dataType: "json",                    
                        url: "<?= $view->action('get_events')?>",
                        data: {'start':date.format(), 'end':date_fin},
                        success: function(data,status,xhr){

                         $( ".list-times" ).remove();   
                         $( ".casting-event" ).append( "<div class='list-times' style= 'display: flex;'><div/>" );
                         
                         var NewDate = new Date();
                         dateVerif = NewDate.toISOString().substr(0, 10);

                        for (var i = 0; i < data.length; i++) {
                            var obj = data[i];

                                if (obj["center"] == RegionName && obj["inscriNbr"] > 0 && dateVerif <= obj["end"].substr(0, 10) ){
                                	daycliked.css('background-color', '#e91e63');
                                    var time_zone="<div class='list-time' id='"+obj["id"]+"' style= 'margin: 10px; padding: 5px 15px; background: #f7f9fb; border: solid 1px; cursor: pointer;'>";
                                    time_zone+="<p style= 'text-align: center; line-height: 16px; margin: 0;'>"+obj["title"]+"</p>";
                                    time_zone+="</div>";
                                    $( ".list-times" ).append(time_zone);
                                 } else if (obj["center"] == RegionName && (obj["inscriNbr"] <= 0 || dateVerif > obj["end"].substr(0, 10))){
                                 	daycliked.css('background-color', '#e91e63');
                                    var time_zone="<div class='list-time' id='"+obj["id"]+"' style= 'margin: 10px; padding: 5px 15px; background: #b8babc; border: solid 1px; cursor: no-drop;' disabled>";
                                    time_zone+="<p style= 'text-align: center; line-height: 16px; margin: 0;'>"+obj["title"]+"</p>";
                                    time_zone+="</div>";
                                    $( ".list-times" ).append(time_zone);

                                 }                            
                            
                            
                         }                            
                        }
                        });
                }

            });            
           $('a.fc-day-grid-event.fc-h-event.fc-event.fc-start.fc-end').css({"border": "1px solid rgb(233, 30, 99)"});
        });

    $(document).on( "click",".list-time" ,function() {
       
       var NewDate = new Date();
       dateVerif = NewDate.toISOString().substr(0, 10);
       var id = $(this).attr('id');
       var item = $('div[data-calendar=<?=$bID?>]').fullCalendar('clientEvents',id);
 	   var dateEvent = item[0].start.toISOString().substr(0, 10)
       if(item[0].inscriNbr>0 && dateVerif <= dateEvent){
           
           var name_event = item[0].title;
           var date_event = item[0].start.toISOString().substr(0, 10);

        $('.list-time').css({"border": "1px solid black"});
        $(this).css({"border": "1px solid rgb(233, 30, 99)"});
        $('.hidden_name').val("");
        $('.hidden_name').val(name_event);
        $('.hidden_date').val("");
        $('.hidden_date').val(date_event);
        $('.hidden_id').val("");
        $('.hidden_id').val(id);
        $('#confirmer_cast').prop('disabled', false);

        }

    });


    </script>
<?php
} ?>
