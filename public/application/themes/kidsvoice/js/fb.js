function statusChangeCallback(response) {

    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        storeFacebookSession();
        fb_getuser();

        if ($('#profile-not-logged').length > 0){
            location.reload();
        }

    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '437186343103816',
        cookie     : true,  // enable cookies to allow the server to access
        // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.2' // use version 2.2
    });

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });

};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function fb_login() {
    FB.login(function(response){
        checkLoginState();
    });
}

function storeFacebookSession(){
    var url = CCM_TOOLS_PATH + '/store_facebook_session';
    $.post( url, {})
        .done(function( data ) {
            console.log(data);
        });
}
function fb_getuser(){
    FB.api('/me?fields=id,name,email', function(response) {
        console.log('Successful login for: ' + response.name);
        if ($('#profile').length > 0){
            $(this).html('HI! '+response.name);
        }
    },{scope: 'email'});
}

function fb_logout(){
    FB.logout(function(response) {
        $.post( CCM_BASE_URL + '/tools/destroy_session')
            .done(function( data ) {
                location.reload();
            });
    });
}
