/*** JS code by 8Ways Media SA -> http://www.8ways.ch/ - 2015 ***/
'use strict';

// device / os / broswer
function osAndBrowserAndDeviceInit() {

	var ua = window.navigator.userAgent.toLowerCase();
	var pl = window.navigator.platform.toLowerCase();
	var ver = window.navigator.appVersion.toLowerCase();
	var device = 'desktop';
	var os = '';
	var browser = 'unknown-browser';

	// devices
	if (ua.indexOf('iphone') > -1) {
		device = 'idevice iphone';
	} else if(ua.indexOf('ipad') != -1) {
		device = 'idevice ipad';
	} else if(ua.indexOf('ipod') != -1) {
		device = 'idevice ipod';
	} else if(ua.indexOf('android') != -1) {
		device = 'android';
	} else if(ua.indexOf('playbook') != -1) {
		device = 'blackberry';
	} else {
		// operating systems
		if(ua.indexOf('mac') != -1) {
			os = 'mac';
		} else if(ua.indexOf('win') != -1) {
			os = 'windows';
		} else if(ua.indexOf('x11') != -1) {
			os = 'unix';
		} else if(ua.indexOf('linux') != -1) {
			os = 'linux';
		}
	}

	// browsers
	if (ua.indexOf('msie') != -1){
		if (ver.indexOf('msie 6.') != -1) {
			browser = 'ie ie6';
		} else if (ver.indexOf('msie 7.') != -1) {
			browser = 'ie ie7';
		} else if (ver.indexOf('msie 8.') != -1) {
			browser = 'ie ie8';
		} else if (ver.indexOf('msie 9.') != -1) {
			browser = 'ie ie9';
		} else if (ver.indexOf('msie 10.') != -1) {
			browser = 'ie ie10';
		} else {
			browser = 'ie';
		}
	} else if(ua.indexOf('trident/7') != -1) {
		browser = 'ie ie11';
	} else if (ua.match(/opera|opr\//)) {
		browser = 'opera';
	} else if (ua.match('crios')) {
		browser = 'ios-chrome';
	} else if (ua.indexOf('chrome') != -1) {
		browser = 'chrome';
	} else if (ua.indexOf('safari') != -1) {
		browser = 'safari';
	} else if (ua.indexOf('firefox') != -1) {
		browser = 'firefox';
	}

	// add class
	var newclasses = device + ' ' + os + ' ' + browser;
	$('html').addClass(newclasses);

};

// IE10 viewport hack for Surface/desktop Windows 8 bug
function ie10ViewportBugWorkaroundInit() {

	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		var msViewportStyle = document.createElement('style');
		msViewportStyle.appendChild(
			document.createTextNode(
				'@-ms-viewport{width:auto!important}'
			)
		);
		document.querySelector('head').appendChild(msViewportStyle);
	}

};

// fake placeholder
function fakePlaceholderInit() {

	if( $('html').hasClass('ie') ) {
		$('[placeholder]').each(function() {
			// add basicly
			var basicVal = $(this).attr('placeholder');
			if( $(this).val() == '' ) { $(this).val(basicVal); }
			// focus event
			$(this).focus(function() {
				if( $(this).val() == basicVal ) { $(this).val(''); }
			});
			// blur event
			$(this).blur(function() {
				if( $(this).val() == '' ) { $(this).val(basicVal); }
			});
		});
	}

};

// only numbers
function onlyNumbersInit() {

	if( $('.only-numbers').length != 0 ) {
		$('.only-numbers').keyup(function() {
			var v = $(this).val();
			$(this).val(v.replace(/\D/g,''));
		});
	}

};

// bg image
function bgImageInit() {

	if( $('.bg-image').length != 0 ) {
		$('.bg-image').each(function() {
			// get data
			var styleData = '';
			if( $(this).attr('style') != undefined ) {
				styleData = $(this).attr('style');
			}
			var imageObject = $(this).find('img');
			var imageLink = imageObject.attr('src');
			var imageAlt = imageObject.attr('alt');
			// setup bg image
			$(this).attr('style', 'background-image: url("'+imageLink+'"); filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'+imageLink+'", sizingMethod="scale"); ' + styleData);
			// add fake text inside, because some browsers doesn't rendering well the empty elements
			imageObject.after('<span class="bg-image-text">'+imageAlt+'</span>');
		});
	}

};

// smooth wheel
function smoothWheelInit() {

	if ( !$('html').hasClass('mac') && $('html').hasClass('desktop') && ( $('html').hasClass('chrome') || $('html').hasClass('safari') || $('html').hasClass('opera') || $('html').hasClass('ie') ) ) {
		jQuery.extend(jQuery.easing, {
			easeOutQuint: function(x, t, b, c, d) {
				return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
			}
		});
		var wheel = false,
		$docH = $(document).height() - $(window).height(),
		$scrollTop = $(window).scrollTop();

		$(window).bind('scroll', function() {
			if (wheel === false) {
				$scrollTop = $(this).scrollTop();
			}
		});
		$(document).bind('DOMMouseScroll mousewheel', function(e, delta) {
			delta = delta || -e.originalEvent.detail / 3 || e.originalEvent.wheelDelta / 30;
			wheel = true;

			$scrollTop = Math.min($docH, Math.max(0, parseInt($scrollTop - delta * 20)));
			$($.browser.webkit ? 'body' : 'html').stop().animate({
				scrollTop: $scrollTop + 'px'
			}, 1000, 'easeOutQuint', function() {
				wheel = false;
			});
			return false;
		});
	}

};

// after resize
var afterResizeInit = (function () {

	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};

})();

function youtubePlayerInit(){

	$('#pause-button').fadeOut();

	$('#play-button').click(function(){
		$('html').addClass('home-youtube-open');
        $('.youtube #video').removeClass('hide');
		$(this).fadeOut();
//		$('#video').attr('src','https://www.youtube.com/embed/updR7Luk210?autoplay=true');
		$('#pause-button').fadeIn();
	});

	$('#pause-button').click(function(){
		$(this).fadeOut();
		$('#play-button').fadeIn();
	});

	$('.youtube-close').click(function(){
		$('html').removeClass('home-youtube-open');

		setTimeout(function () {
            $('.youtube #video').addClass('hide');
        }, 500);

		$('#pause-button').fadeOut();
		$('#play-button').fadeIn();
	});

};

function youtubePlaylistInit(){

	// define variables
	var playlistArray = $('#youtube-playlist-holder').data('videos-to-play');
	var playlistArrayLenght = playlistArray.length;
	var i = 0;

	// default values to the elements
	$('#playlist-container .title p').text(playlistArray[0].video_name);
	$('#playlist-container iframe').attr('src',playlistArray[0].video_url);
	$('#playlist-previous-button').addClass('inactive');

	$('#playlist-pause-button').fadeOut();

	$('#playlist-play-button').click(function(){
		$('html').addClass('home-youtube-open');
		$(this).fadeOut();
		$('#playlist-pause-button').fadeIn();
	});

	$('#playlist-pause-button').click(function(){
		$(this).fadeOut();
		$('#playlist-play-button').fadeIn();
	});

	// next button
	$('#playlist-next-button').click(function(){

		$('#playlist-play-button').fadeIn();
		$('#playlist-pause-button').fadeOut();

		i++;

		$('#playlist-container .title p').text(playlistArray[i].video_name);
		$('#playlist-container iframe').attr('src',playlistArray[i].video_url);

		$('#playlist-previous-button').removeClass('inactive');
		if ( i == playlistArrayLenght-1 ){
			$(this).addClass('inactive');
		}

	});

	// previous button
	$('#playlist-previous-button').click(function(){

		$('#playlist-play-button').show();
		$('#playlist-pause-button').hide();

		i--;

		$('#playlist-container .title p').text(playlistArray[i].video_name);
		$('#playlist-container iframe').attr('src',playlistArray[i].video_url);

		$("#playlist-next-button").removeClass('inactive');
		if ( i == 0 ){
			$(this).addClass('inactive');
		}

	});

	$('#playlist-play-button').show();
	$('#playlist-pause-button').show();

};

function videoGalleryInit(){

	var videoGallerySlider = $(".video-gallery-slider");

	videoGallerySlider.owlCarousel({
		pagination : true,
		navigation: false,
		items: 3
	});

};

function welcomeSliderInit(){

	if( !$('body.admin').hasClass('edit') ) {

        var welcomeSlider = $(".welcome-slider");

        if( welcomeSlider.length != 0 ) {

            var jurySliderAutoplaySpeed = welcomeSlider.attr('data-autoplaySpeed');

            welcomeSlider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
				prevArrow: '<div class="slick-arrow slick-arrow-prev"><i class="fa fa-caret-left"></i></div>',
				nextArrow: '<div class="slick-arrow slick-arrow-next"><i class="fa fa-caret-right"></i></div>',
                dots: false,
                autoplaySpeed: jurySliderAutoplaySpeed,
                autoplay: true,
                pauseOnHover: true,
                infinite: true
            });

        }

    }


};

function welcomeSliderHelper() {
    if( $(window).width() > 767 ) {
        $('.welcome-slider-item-text').height( $('.welcome-slider-item-text-wrap').height() );

        $('.welcome-slider-item-text-wrap').css( 'margin-top', -( ($('.welcome-slider-item-text').height() / 2) - 10 ) );

    } else {
        $('.welcome-slider-item-text').height('auto');
	}
}

function jurySliderInit(){

	var jurySlider = $(".jury-slider");

	if( jurySlider.length != 0 ) {

        jurySlider.slick({
            adaptiveHeight: true,
        	dots: false,
			arrows: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            pauseOnHover: true,
            autoplay: true,
			variableWidth: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                    	centerMode: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
						variableWidth: false
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

    }

};


// syncCarouselInit
function syncCarouselInit(){

	var sync1 = $("#sync1");
	var sync2 = $("#sync2");

	sync1.owlCarousel({
		singleItem: true,
		slideSpeed: 1000,
		navigation: true,
		navigationText: ['<i class="fa fa-caret-left"></i>','<i class="fa fa-caret-right"></i>'],
		pagination: false,
		afterAction: syncPosition,
		responsiveRefreshRate: 200,
		autoHeight: true,
        lazyLoad: true,
	});

	sync2.owlCarousel({
		items: 4,
		itemsDesktop: [1199, 4],
		itemsDesktopSmall: [979, 4],
		itemsTablet: [768, 4],
		itemsMobile: [479, 2],
		pagination: false,
		responsiveRefreshRate: 100,
		afterInit: function (el) {
			el.find(".owl-item").eq(0).addClass("synced");
		},
        lazyLoad: true,
	});

	function syncPosition(el) {
		var current = this.currentItem;
		$("#sync2")
			.find(".owl-item")
			.removeClass("synced")
			.eq(current)
			.addClass("synced")
		if ($("#sync2").data("owlCarousel") !== undefined) {
			center(current)
		}
	}

	$("#sync2").on("click", ".owl-item", function (e) {
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo", number);
	});

	function center(number) {
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;
		for (var i in sync2visible) {
			if (num === sync2visible[i]) {
				var found = true;
			}
		}

		if (found === false) {
			if (num > sync2visible[sync2visible.length - 1]) {
				sync2.trigger("owl.goTo", num - sync2visible.length + 2)
			} else {
				if (num - 1 === -1) {
					num = 0;
				}
				sync2.trigger("owl.goTo", num);
			}
		} else if (num === sync2visible[sync2visible.length - 1]) {
			sync2.trigger("owl.goTo", sync2visible[1])
		} else if (num === sync2visible[0]) {
			sync2.trigger("owl.goTo", num - 1)
		}

	}
};

// header open
function headerOpenInit() {

	// menu open/close
	$('.menu-open-close').click(function() {
		$('html').toggleClass('open-menu');
	});

};

// fancyboxInit
function fancyboxInit(){

	if ( $('.fancybox').length != 0  ){

		if ( $(window).width() > 768 ){
			$(".fancybox").fancybox({
				autoResize: true,
				helpers: {
					title: null
				}
			});
		} else {
			$(".fancybox").fancybox({
				autoResize: true,
				helpers: {
					title: null
				},
				'onUpdate' : function() {
					$.fancybox.update();
				},
			});
		}

	}
};

// peanuts
function peanuts(){

	$('.data-list a').click(function(){
		return false;
	});

	if ( $('.selectbox').length != 0  ){
		$(".selectbox").selectbox();
	}

	/*if ( $(window).width() > 1024 ){
		if ( $('#welcome').length != 0  ){
			$('#welcome').parallax();
		}
	}*/

	$('.hassubmenu').click(function(event){

		if ( $(window).width > 1024 ){
			event.preventDefault();
		} else {
			$(this).parent().toggleClass('open');
			event.preventDefault();
		}

	});

	if ( $('.four-oh-four').length != 0 ) {
		 var windowHeight = $(window).height();
		 var headerHeight = $('.header').height();
		 var footerHeight = $('.footer').height();

		 $('.four-oh-four').css('height',windowHeight-headerHeight-footerHeight);
		 $('.four-oh-four').css('line-height',windowHeight-headerHeight-footerHeight+'px');
	}

	$('.hassubmenu').parent().addClass('hassubmenuLi');

};

// skrollrInit
function skrollrInit(){

	if( $('.header').length != 0 ) {

		// desktop
		if( $('html').hasClass('desktop') && $(window).width() > 1024 ) {

			var windowHeight = $(window).height();

			// setup
			$('.header')
				.attr('data-0', 'background-color:rgba(189,24,109,0);')
				.attr('data-'+windowHeight+'', 'background-color:rgba(189,24,109,0.99)');
			// setup
			$('.profile')
				.attr('data-0', 'background-color:rgba(179,20,107,0);')
				.attr('data-'+windowHeight+'', 'background-color:rgba(179,20,107,0.99)');

			// init skrollr
			skrollr.init({
				smoothScrolling: false,
				forceHeight: false
			});

		}

		// mobile devices
		else {
			if( $('html').hasClass('skrollr') ) {
				skrollr.init().destroy();
			}
		}

	}

};

// fake placeholder
function fakePlaceholderInit() {

    $('input, textarea').placeholder();

};

// welcomeImageInit
function welcomeImageInit(){

	if ( $(window).width() > 1024 ){

		 $('#welcome .bg-image img').each(function(index, value){

			var welcomeDataImg = $(this).attr('data-src');
			$(this).attr('src', welcomeDataImg);

		});

	}

};

// equalHeights
function equalHeights(elem_name, child_name){

	var maxHeight = 0;
	var elem = $(''+elem_name+'');

		if ( child_name ){
			var maxChildHeight = 0;

				elem.children(''+child_name+'').css('min-height','auto');
				elem.each(function(index){
					var thisHeight = $(this).children(''+child_name+'').outerHeight();
					if ( thisHeight >= maxChildHeight ){
						maxChildHeight = thisHeight;
					}
				});
				elem.children(''+child_name+'').css('min-height',maxChildHeight);
		}

		elem.css('min-height','auto');
		elem.each(function(index){
			var thisHeight = $(this).outerHeight();
			if ( thisHeight >= maxHeight ){
				maxHeight = thisHeight;
			}
		});
		elem.css('min-height',maxHeight);

};

	// equalHeightInits
	function equalHeightInits(){
		if ( $(window).width() > 767 ){
			equalHeights('.news .col-md-6');
		}
	};

// same height
function sameHeightInit() {

	// get data
	var $sameHeightContainer = $('[data-same-height]');

	if( $sameHeightContainer.length != 0) {

		$sameHeightContainer.each(function() {

			// tablet + desktop
			if( $(window).width() >= 768 ) {

				var $containerID = $(this).attr('data-same-height');

				var $minHeight = 0;

				$('[data-same-height="' + $containerID + '"]').each(function() {

					$(this).css('min-height', 'auto').css('line-height', 'normal');
					var $currentHeight = $(this).outerHeight();
					if ( $currentHeight >= $minHeight ){
						$minHeight = $currentHeight;
					}

				}).css('min-height', $minHeight).css('line-height', $minHeight+'px');

			}

			// mobile
			else {

				$(this).css('min-height', 'auto').css('line-height', 'normal');

			}

		});

	}

};


function soundwaveInit() {

    var wrap = document.getElementById('soundwave');

    if( window.innerWidth >= 1200 ) {

        var windowWidth = (window.innerWidth > 1920 ? 1920 : window.innerWidth);
        var barWidth = 1;
        var barSpaceBetween = 2;
        var barCount = Math.round(windowWidth / (barWidth + barSpaceBetween));
        var nodeBar;

        wrap.style.width = windowWidth + 'px';
        wrap.style.left = '50%';
        wrap.style.marginLeft = -(windowWidth / 2) + 'px';

        for (var i = 0; i < barCount; i++) {

            if ( barCount > parseInt(windowWidth) / 2 ) {
                var animationDur = Math.floor((Math.random() * 10) + i / 1000) + 's';
                var animationDel = '0.' + Math.floor((Math.random() * 5) + i) + 's';
            } else {
                var animationDur = Math.floor((Math.random() * 5)) + 's';
                var animationDel = '0.' + Math.floor((Math.random() * 10)) + 's';
            }

            nodeBar = document.createElement("div");
            nodeBar.classList.add('bar');
            wrap.appendChild(nodeBar);
            nodeBar.style.animationDuration = animationDur;
            nodeBar.style.animationDelay = animationDel;

        }

    }


	// Canvas animation
	/*if( !$('html').hasClass('ie') ) {

		var canvas = document.getElementById("myCanvas");

		if( canvas != null ) {
			var ctx = canvas.getContext("2d");
			var bar = [];
			var fps = 1;
			var maxBarHeight = 300;//30;
			var minBarHeight = 10;//15
			var barPadding = 0;//space between bars
			var barWidth = 1; //width of each bar
			var totalBars = window.innerWidth;//total bars to create
			var barColor = '#d5d3d5';
		}

    }

    function InitBars()
    {
		canvas.width = window.innerWidth;
		canvas.height = maxBarHeight;
		canvas.style.top = '50%';
		canvas.style.marginTop = -(maxBarHeight / 2) + 'px';

        var barHeight = 0;
        var x = 0;
        var y = 0;
        for(var i=0; i<totalBars; i++)
        {
            barHeight = RandomizeHeight();
            x = (i + i) + barPadding;
            y = ComputeYAxis(barHeight);
            bar.push({
                height: barHeight,
                buffHeight: barHeight,
                x: x,
                y: y
            });

            ShrinkBar(i);
        }
    }
    function ShrinkBar(index)
    {
        if(bar[index].height > minBarHeight)
        {
            ctx.fillStyle = barColor;
            ctx.clearRect(bar[index].x, bar[index].y, barWidth, bar[index].height);
            bar[index].height--;
            bar[index].y = ComputeYAxis(bar[index].height);
            ctx.fillRect(bar[index].x, bar[index].y, barWidth, bar[index].height);
            setTimeout(function() {
				requestAnimationFrame(function(){
					ShrinkBar(index)
				})
            }, (1 / fps) * index);
        }
        else
        {
            var newHeight = RandomizeHeight();
            while(newHeight==minBarHeight)
            {
                newHeight = RandomizeHeight();
            }
            bar[index].buffHeight = newHeight;
            GrowBar(index);
        }

    }
    function GrowBar(index)
    {
        if(bar[index].height < bar[index].buffHeight)
        {
            ctx.fillStyle = barColor;
            ctx.clearRect(bar[index].x, bar[index].y, barWidth, bar[index].height);
            bar[index].height++;
            bar[index].y = ComputeYAxis(bar[index].height);
            ctx.fillRect(bar[index].x, bar[index].y, barWidth, bar[index].height);

            setTimeout(function() {
				requestAnimationFrame(function(){
					GrowBar(index)
				})
            }, (1 / fps) * index);
        }
        else
        {
            ShrinkBar(index);//shrink it again
        }

    }
    function RandomizeHeight()//generate random seed number
    {
        return Math.floor((Math.random() * maxBarHeight) + minBarHeight);
    }
    function ComputeYAxis(height)//computes the position of the y
    {
        return Math.round((maxBarHeight - height)/2);
    }

    if( !$('html').hasClass('ie') ) {
		if( canvas != null ) {
			InitBars();
		}
	}*/

}

// --- Ready
$(document).ready(function() {

	// welcomeImageInit
	welcomeImageInit();

	// device / os / broswer
	osAndBrowserAndDeviceInit();

	// IE10 viewport hack for Surface/desktop Windows 8 bug
	ie10ViewportBugWorkaroundInit();

	// fake placeholder
	fakePlaceholderInit();

	// only numbers
	onlyNumbersInit();

	// headerOpenInit
	headerOpenInit();

	// youtubePlayerInit
	youtubePlayerInit();

	// youtubePlaylistInit
//	youtubePlaylistInit();

	// owlSliderInit
    welcomeSliderInit();

    //welcomeSliderHelper();

	// owlSliderInit
    jurySliderInit();

	// videoGalleryInit();
	//videoGalleryInit();

	// skrollrInit
	//skrollrInit();

	// peanuts
	peanuts();

	// fancyboxInit
	fancyboxInit();

	// syncCarouselInit
	syncCarouselInit();

	// sameHeightInit
	sameHeightInit();

    soundwaveInit();

});

// --- Load
$(window).load(function() {

	// bg image
	bgImageInit();

	// videoGalleryInit
	//setTimeout(function(){
		//videoGalleryInit();
	//}, 1000);

	// smooth wheel
	//smoothWheelInit();
	// if ajax in use, refresh needed in ever seconds!
	//setInterval(function() { smoothWheelInit(); }, 1000);

	// sameHeightInit
	sameHeightInit();

});

// --- Resize
$(window).resize(function() {

	// after resize
	afterResizeInit(function() {

		// fancyboxInit
		fancyboxInit();

		// smooth wheel
		//smoothWheelInit();

		// sameHeightInit
		sameHeightInit();

        //welcomeSliderHelper();

	}, 100);

});
