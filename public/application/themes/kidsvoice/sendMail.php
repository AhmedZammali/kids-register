<?php
session_start();
?>

<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$lang = Localization::activeLanguage();
	$prenom = $_SESSION["prenom"];
	$nom = $_SESSION["nom"];
	$mail = $_SESSION["mail"];
	$mobile = $_SESSION["mobile"]; 
	$age = $_SESSION["age"]; 
	$adresse = $_SESSION["adresse"]; 
	$postale = $_SESSION["postal"];
	$ville = $_SESSION["ville"]; 
	$region = $_POST['region'];
	$id_event = $_POST['hidden_id'];
	$name_event = $_POST['hidden_name'];
	$date_event = $_POST['hidden_date'];
	$cle = md5(microtime(TRUE)*100000);
	$actif = 0;

    $db = \Database::connection();
  
     		$db->Execute('INSERT INTO casting VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
		    array(
		    	null,
		         $nom,
		         $prenom,
		         $mail,
		         $mobile,
		         $age,
		         $adresse,
		         $postale,
		         $ville,
		         $region,
		         $name_event,
		         $date_event,
		         $cle,
		         $actif
		     ));






        //sending verification mail
			$url = View::url('/').'/'.$lang.'/activation?mail='.urlencode($mail).'&cle='.urlencode($cle).'&event='.urlencode($id_event).'&lang='.urlencode($lang);
			$subject = t("Activer votre compte");
			$body = t("

Cher Participant,

Pour activer votre compte, veuillez cliquer sur le lien ci-dessous.

Vous pouvez également le copier / coller dans votre navigateur internet.

Merci et à bientôt.

L'équipe du Swiss Voice Tour.

").$url;


			$mh = Loader::helper('mail');
			$mh->setSubject($subject);
			$mh->setBody($body);
			$mh->to($mail, $nom);
			$mh->from('noreply@kidsvoice.ch');
			$mh->sendMail();


			// suppression variable de session + redirection avec message 
			unset($_SESSION['prenom']);
			unset($_SESSION['nom']);
			unset($_SESSION['mail']);
			unset($_SESSION['mobile']);
			unset($_SESSION['age']);
			unset($_SESSION['adresse']);
			unset($_SESSION['postal']);
			unset($_SESSION['ville']);

			$_SESSION["message_retour"] = t('Merci de consulter votre boîte mail pour activer votre compte.');
			$site_url = View::url('/');
            $site_url .='/'.$lang."#inscri";
?>
			<script>
				//alert("<?php //echo json_encode($site_url); ?>");
				var url_js = <?php echo json_encode($site_url); ?>;
				window.location.replace(url_js) ;
			</script>
<?php } ?>

