<?php
// Start the session
session_start();
?>

<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header-inscri.php');
$lang = Localization::activeLanguage();
$requestType = $_SERVER['REQUEST_METHOD'];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_SESSION["prenom"] = $_POST['vFirstName'];
	$_SESSION["nom"] = $_POST['vLastName'];
	$_SESSION["mail"] = $_POST['vEmail'];
	$_SESSION["mobile"] = $_POST['vMobileNumber'];
	$_SESSION["age"] = $_POST['vAge'];
	$_SESSION["adresse"] = $_POST['vAddress'];
	$_SESSION["postal"] = $_POST['vPostalCode'];
	$_SESSION["ville"] = $_POST['vCity'];
} else { 
		$site_url = View::url('/');
		$site_url.= '/'.$lang;
        $uinfo = new User();
	if (!($uinfo->IsLoggedIn())) { 
?>
			<script>
				var url_js = <?php echo json_encode($site_url); ?>;
				window.location.replace(url_js) ;
			</script>
<?php
	}
}

$u = new User();
    if($u->isLoggedIn()) {
?>        
<div class="page contents col-xs-12">
<?php } else { ?> 
<div class="page contents col-xs-12" style="top: 126px;">   
    <?php } 
    $this->inc('elements/subpage_header.php'); ?>
    <div class="subpage-content border-bottom row clearfix padding-vertical-default">
        <div class="container texts">
            <div class="col-md-6 left">
                <?php $as = new Area('Main Content'); ?>
                <?php $as->display($c); ?>
            </div>
            <div class="col-md-6 right">
                <?php $as = new Area('Content'); ?>
                <?php $as->display($c); ?>
                
            </div>
        </div>

    <div class="subpage-content row clearfix padding-vertical-default">
        <div class="container texts">
            <div class="col-md-12">
                <?php $as = new Area('reglement'); ?>
                <?php $as->display($c); ?>
                <div id="inscri" style="height: 2px;"></div>   
            </div>
        </div>
    </div>            

<section _ngcontent-c0="" id="coming-soon">
	<div _ngcontent-c0="" class="container-fluid gradient-flickr coming-soon-bg ">
		<div _ngcontent-c0="" class="row min-height">
		    <div _ngcontent-c0="" class="col-md-7 formprofile">
		        <div _ngcontent-c0="" class="card">
		            <div _ngcontent-c0="" class="card-body">
			            <div _ngcontent-c0="" class="px-3">


				            <form _ngcontent-c0="" class="form ng-valid ng-touched ng-dirty" method="post" action="<?php echo View::url('/').'/'.$lang; ?>/send-mail">

								<!--select form -->
						        <div class="form-group col-md-12">
						        	<div class="col-md-5">
						        	<label for="sel1"><?=t('Choisissez Un Centre:')?></label>
	                                		<select class="form-control " id="sel1" name="region">
	                                			<option value=""><?php echo t('Sélectionnez un centre:'); ?></option>
							            		<?php 
							            			$db = \Database::connection();
		   											$foo = $db->GetAll('SELECT value FROM atSelectOptions WHERE avSelectOptionListID = 2 And isDeleted = 0');
		 											foreach ($foo as $key => $value) {?>
		 									 			<option value="<?=$value['value'];?>" class="input-lg"><?=$value['value'];?></option>
		 									 	<?php } ?>
		 									</select>
		 							</div>
						        </div>


				                <div _ngcontent-c0="" class="form-body col-md-12">
				                	<div _ngcontent-c0="" class="row">
                    					
                    					<div _ngcontent-c0="" class="col-md-5 datepicker-media">
                    						<div _ngcontent-c0="" class="card-body">
                        						<div _ngcontent-c0="" class="card-block datepicker-cust">
                        							<?php
														$a = new GlobalArea('calendar');
														$a->setCustomTemplate('calendar','templates/casting');
														$a->display($c);
													?>
                        						</div>
                        					</div>
                    					</div>

                    					<div _ngcontent-c0="" class="col-md-7 casting-event">

                    					</div>


                    				</div>
                    				<input type="text" class="hidden_id" name="hidden_id" hidden="">
                    				<input type="text" class="hidden_name" name="hidden_name" hidden="">
                    				<input type="text" class="hidden_date" name="hidden_date" hidden="">
                                    <div _ngcontent-c0="" class="form-actions">
                                        <button _ngcontent-c0="" id="confirmer_cast" class="btn btn-raised btn-raised btn-danger sub-button mr-1" type="submit" disabled="">
                                           <?=t('Confirmer inscription')?>
                                        </button>

                                    </div>
				                </div>

				            </form>
			        	</div>
		    		</div>
				</div>
			</div>
		</div>

	</div>
</section>

</div>
    <?php $this->inc('elements/video_gallery.php'); ?>
    <?php //$this->inc('elements/playlist.php'); ?>
            <div class="col-md-12 left">
                <?php $as = new Area('bottom area'); ?>
                <?php $as->display($c); ?>
            </div>

</div>
<?php $this->inc('elements/footer-inscri.php'); ?>