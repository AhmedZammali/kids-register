<?php defined('C5_EXECUTE') or die("Access Denied"); ?>
<?php $this->inc('elements/header-inscri.php'); 
$lang = Localization::activeLanguage();
$u = new User();
    if($u->isLoggedIn()) {
?>        
<div class="page contents col-xs-12">
<?php } else { ?> 
<div class="page contents col-xs-12" style="top: 126px;">   
    <?php } 
    $this->inc('elements/subpage_header.php'); ?>
    <div class="subpage-content border-bottom row clearfix padding-vertical-default">
        <div class="container texts">
            <div class="col-md-6 left">
                <?php $as = new Area('Main Content'); ?>
                <?php $as->display($c); ?>
            </div>
            <div class="col-md-6 right">
                <?php $as = new Area('Content'); ?>
                <?php $as->display($c); ?>
               
            </div>

        </div>

    <div class="subpage-content row clearfix padding-vertical-default">
        <div class="container texts">
            <div class="col-md-12">
                <?php $as = new Area('reglement'); ?>
                <?php $as->display($c); ?>
                <div id="inscri" style="height: 2px;"></div> 
            </div>
        </div>
    </div>      
<section _ngcontent-c0="" id="coming-soon">
    <div _ngcontent-c0="" class="container-fluid gradient-flickr coming-soon-bg ">
        <div _ngcontent-c0="" class="row min-height">
            <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12 formprofile">
                <div _ngcontent-c0="" class="card">
                    <div _ngcontent-c0="" class="card-body">
                        <div _ngcontent-c0="" class="px-3">
  


                            <form _ngcontent-c0="" class="form first-etape ng-valid ng-touched ng-dirty" method="post" action="<?php echo View::url('/').'/'.$lang; ?>/calendar#inscri">
                                <?php if (isset($_SESSION["message_retour"]) || isset($_SESSION["message_retour_valid"]) || isset($_SESSION["message_retour_valid1"])): ?>
                                <div _ngcontent-c0="" class="align-self-center halfway-fab text-center">
                                    <div class="message de retour" style="font-size: 20px; font-weight: 500; color: #16bf16;">
                                        <?php
                                        echo $_SESSION["message_retour"];
                                        unset($_SESSION['message_retour']);
                                        echo $_SESSION["message_retour_valid"].'<br>';
                                        echo $_SESSION["message_retour_valid1"];
                                        unset($_SESSION['message_retour_valid']);
                                        unset($_SESSION['message_retour_valid1']);
                                        ?>

                                    </div>
                                </div>
                                 <?php else: ?>
                                <div _ngcontent-c0="" class="form-body" >
                                    <div _ngcontent-c0="" class="row">
                                        <div _ngcontent-c0="" class="col-md-12 datepicker-media">
                                            <div _ngcontent-c0="" class="form-body">
                                                <div _ngcontent-c0="" class="row">
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput1"><?=t('Prénom')?></label>
                                                            <input  class="form-control ng-pristine ng-invalid ng-touched" id="projectinput1" name="vFirstName" required tabindex="1" type="text">
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput2"><?=t('Nom')?></label>
                                                            <input _ngcontent-c0="" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput2" name="vLastName" required tabindex="2" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div _ngcontent-c0="" class="row">
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput3"><?=t('E-mail')?></label>
                                                            <input _ngcontent-c0="" autocomplete="off" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput3" name="vEmail" required tabindex="3" type="email" ng-reflect-pattern="[a-zA-Z0-9.-_+]{1,}@[a-zA-Z.-]">
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput4"><?=t('Mobile')?></label>
                                                            <input _ngcontent-c0="" autocomplete="off" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput4" name="vMobileNumber"  required tabindex="4" type="number">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div _ngcontent-c0="" class="row">
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput3"><?=t('âge')?></label>
                                                            <input _ngcontent-c0="" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput3" maxlength="3" minlength="1" name="vAge" tabindex="5" type="number" min="3" max="150" required>
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput4"><?=t('Adresse')?></label>
                                                            <input _ngcontent-c0="" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput4" name="vAddress" required tabindex="6" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div _ngcontent-c0="" class="row">
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput3"><?=t('code postal')?></label>
                                                            <input _ngcontent-c0="" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput3" name="vPostalCode"required tabindex="7" type="number" min="1" max="99999">
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-c0="" class="col-md-6 col-sm-12 col-xs-12">
                                                        <div _ngcontent-c0="" class="form-group">
                                                            <label _ngcontent-c0="" for="projectinput4"><?=t('ville')?></label>
                                                            <input _ngcontent-c0="" class="form-control ng-untouched ng-pristine ng-invalid" id="projectinput4" name="vCity" required tabindex="8" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div _ngcontent-c0="" class="form-actions">
                                        <button _ngcontent-c0="" class="btn btn-danger mr-1" required="" tabindex="10" type="button">
                                            <?=t('Effacer')?>
                                        </button>
                                        <button _ngcontent-c0="" class="btn btn-primary" required="" tabindex="9" type="submit">
                                           <?=t('Suivant')?>
                                        </button>

                                    </div>
                                </div>
                                <?php endif; ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
       
 </div>
    <?php $this->inc('elements/video_gallery.php'); ?>
    <?php //$this->inc('elements/playlist.php'); ?>
            <div class="col-md-12 left">
                <?php $as = new Area('bottom area'); ?>
                <?php $as->display($c); ?>
            </div>

</div>
 
<script type="text/javascript">
    $(".btn.btn-danger.mr-1").click(function(){
        $(".form.first-etape.ng-valid.ng-touched.ng-dirty").get(0).reset();
    });
</script>

<?php $this->inc('elements/footer-inscri.php'); ?>
 