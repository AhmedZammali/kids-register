<?php

defined('C5_EXECUTE') or die("Access Denied");

$user = new User();

?>

<div class="subpage-title row">
<!--    <canvas id="myCanvas">Your browser does not support the HTML5 canvas tag.</canvas>-->

    <div class="soundwave-wrap" id="soundwave"></div>

    <div class="container texts text-center">
        <h1>
            <?php if ($c->getCollectionTypeHandle() == 'voter_profile' && $user->isLoggedIn()) : ?>
                <?php echo t('Hello'); ?> <?php echo UserInfo::getByID($user->getUserID())->getAttribute('full_name'); ?>
            <?php elseif ($c->getCollectionPath() == '/sms_auth') : ?>
                <?php echo t('Sms auth'); ?>
            <?php else: ?>
                <?php echo $c->getCollectionName(); ?>
            <?php endif; ?>
        </h1>
    </div>
</div>
