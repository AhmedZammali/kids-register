<div class="casting row bg-color-light-gray5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="casting-message">
                    <?php $area = new Area('Citation'); ?>
                    <?php $area->display($c); ?>
                </div>

                <div class="casting-links">
<!--                    <a href="https://www.youtube.com/embed/8pkrMMqkAwI" class="btn btn-link fancybox" data-fancybox-type="iframe" title="--><?php //echo t('Voir La Video') ?><!--">--><?php //echo t('Voir La Video') ?><!--</a>-->
                    <?php $area = new Area('YouTube Video'); ?>
                    <?php $area->display($c); ?>
                    <?php $area = new Area('MP3 File'); ?>
                    <?php $area->display($c); ?>
                </div>
            </div>
        </div>
    </div>
</div>