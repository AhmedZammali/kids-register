<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<ul class="tour-list">
    <?php Loader::model('dashboard/voting_system/shops/model'); ?>
    <?php $model = new DashboardVotingSystemShopsModel(); ?>
    <?php $shops = $model->getAllShops(); ?>
    <?php foreach ($shops as $shop){ ?>
        <li>
            <?php if ($shop['vss_logo']){ ?>
                <div class="col-sm-2">
                    <img src="<?php echo File::getByID($shop['vss_logo'])->getVersion()->getRelativePath(); ?>" alt="<?php echo $shop['vss_name']; ?>">
                </div>
            <?php } ?>
            <div class="col-sm-5">
                <p><?php echo $shop['vss_name']; ?></p>
            </div>
            <div class="col-sm-3">
                <p><?php echo $shop['vss_date']; ?></p>
            </div>
            <div class="col-sm-2 text-right">
                <p>
                    <a target="_blank" href="<?php echo $shop['vss_website']; ?>" title="<?php echo t('Voir le site'); ?>"><?php echo t('Voir le site'); ?></a>
                </p>
            </div>
        </li>
    <?php } ?>
    <?php $as = new Area('Additional Tour'); ?>
    <?php $as->display($c); ?>
</ul>