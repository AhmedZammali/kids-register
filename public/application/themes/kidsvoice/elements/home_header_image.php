<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<div id="welcome" class="welcome row">

    <div class="soundwave-wrap" id="soundwave"></div>

    <?php $area = new Area('Home Slider'); ?>
    <?php $area->display($c); ?>

</div>