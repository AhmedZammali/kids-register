<?php

defined('C5_EXECUTE') or die("Access Denied");

$container = \DependencyInjection\Container::getInstance();

$language = $container->getLanguage();

?>

<div class="clearfix"></div>

<div class="partners row texts text-center">
    <div class="container padding-vertical-small">
        <nav class="partners-list">
           <div class="col-sm-2"></div>
            <ul class="col-sm-8">
                <?php $as = new GlobalArea($language . ' Home Partners'); ?>
                <?php $as->setBlockWrapperStart('<li class="col-md-4">'); ?>
                <?php $as->setBlockWrapperEnd('</li>'); ?>
                <?php $as->display($c); ?>
            </ul>
        </nav>
        <div class="clearfix"></div>
    </div>
</div>

<footer class="footer bg-color-dark-purple texts">

    <?php $this->inc('elements/footer/socials.php'); ?>

    <div class="container">
        <div class="row">
            <div class="vertical-middle">
                <div class="col-md-6 col-xs-12 sm-text-center xs-text-center">
                    <p>&copy; <?php echo t('Kids Voice Tour'); ?> - 
                 <?php
                 $as = new GlobalArea($language . ' Privacy Policy'); 
               $as->display($c); 
                ?>


                    </p>

                </div>

                <div class="col-md-6 col-xs-12 text-right sm-text-center xs-text-center">
                    <p>Created by: <a href="http://www.8ways.ch/" title="8 Ways Media - web design agency in Geneva" target="_blank">8Ways Media SA <span class="icomoon-8-ways"></span></a></p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- footer END -->

</div>

<?php if ($c->getCollectionTypeHandle() == 'home'){ ?>
    <?php $this->inc('elements/home_js.php'); ?>
<?php } ?>

<!-- main END -->

<?php $this->inc('elements/footer/assets.php',array(
    'themePath' => $this->getThemePath()
)); ?>

<?php Loader::element('footer_required'); ?>

</body>
</html>
