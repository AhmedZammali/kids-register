<?php

defined('C5_EXECUTE') or die("Access Denied");

$container = \DependencyInjection\Container::getInstance();

$language = $container->getLanguage();

$navigation = $container->getNavigationService();
$u = $container->getCurrentUser();

?>

<!DOCTYPE html>
<html lang="<?php echo $container->getLanguage(); ?>">
<head>

    <!-- Setup charset -->
    <meta charset="utf-8">

    <!-- Setup viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

    <!-- No automatic telephone detecting on iPad -->
    <meta name="format-detection" content="telephone=no">

    <!-- IE compatible mode -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <?php $this->inc('elements/header/og.php',array(
        'siteName' => SITE,
        'title' => $c->getCollectionName(),
        'url' => $navigation->getCollectionURL($c),
        'image' => $c->getAttribute('image') ? BASE_URL.''.$c->getAttribute('image')->getVersion()->getRelativePath() : BASE_URL.''.$this->getThemePath().'/img/base/logo-200x200.png'
    )); ?>

    <?php Loader::element('header_required'); ?>

    <?php $this->inc('elements/header/assets.php',array(
        'themePath' => $this->getThemePath()
    )); ?>

    <script>var CCM_LOCALE = "<?php echo $container->getLocale(); ?>";</script>
</head>
<body class="<?php echo (User::isLoggedIn()) ? 'admin' : ''; echo ($c->isEditMode()) ? ' edit' : ''; ?>">

<!-- Conversion Pixel - Coop KidsVoice [VISITS] - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/px?id=752342&seg=6091428&t=1" type="text/javascript"></script>
<!-- End of Conversion Pixel -->

<?php $this->inc('elements/header/fb_js_sdk.php',array()); ?>

<!-- main START -->
<div class="main" style="">
    <!-- header START -->
    <?php if ($u->isSuperUser() || $u->inGroup(Group::getByName('Administrators'))){ $margin = 'margin-top:50px'; }else{ $margin = 0; } ?>
    <header class="header border-bottom">
        <div class="bg-color-dark-purple">
            <div class="container">
                <div class="row header-top">
                    <div class="col-sm-6 col-xs-12 texts">
                        <?php $area = new GlobalArea('Social Links'); ?>
                        <?php $area->display($c); ?>
                    </div>

                    <div class="col-sm-6 texts lg-text-right">
                        <div class="header-top-right">
                            <?php $area = new GlobalArea($language . ' Connexion'); ?>
                            <?php $area->display($c); ?>

                            <?php $as = new GlobalArea('Language Selector'); ?>
                            <?php $as->display($c); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-color-light-gray2">
            <div class="container" style="<?php echo $margin; ?>">
                <?php $as = new GlobalArea($language.' Main Menu'); ?>
                <?php $as->display($c); ?>
            </div>
        </div>
    </header>
