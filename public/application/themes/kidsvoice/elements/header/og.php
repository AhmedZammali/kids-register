<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<meta property="og:site_name" content="<?php $siteName ?>">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $url; ?>">
<meta property="og:image" content="<?php echo $image; ?>">
