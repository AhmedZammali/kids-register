<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<!-- Vendor CSS -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $themePath; ?>/css/vendor.min.css">

<!-- Project CSS -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $themePath; ?>/css/kidsvoice.css?v=006">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $themePath; ?>/css/area_color_fix.css">

<!-- Owl Carousel CSS -->
<!--<link rel="stylesheet" type="text/css" media="all" href="--><?php //echo $themePath; ?><!--/js/owl-carousel/owl.carousel.css">-->
<!--<link rel="stylesheet" type="text/css" media="all" href="--><?php //echo $themePath; ?><!--/js/owl-carousel/owl.theme.css">-->

<!-- Slick CSS -->
<!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>-->

<!-- Selectbox CSS -->
<!--<link rel="stylesheet" type="text/css" media="all" href="--><?php //echo $themePath; ?><!--/js/selectbox/jquery.selectbox.css">-->

<!-- Fancybox CSS -->
<!--<link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" />-->

<!-- Basic icons -->
<link rel="icon" href="<?php echo $themePath; ?>/img/base/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo $themePath; ?>/img/base/logo-200x200.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
