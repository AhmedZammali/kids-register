<?php

defined('C5_EXECUTE') or die("Access Denied");

?>
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '612920592143488',
            xfbml      : true,
            version    : 'v2.6',
            status     : true
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- TODO Check if we use this -->
<!--<div id="fb-root"></div>-->
<!--<script>(function(d, s, id) {-->
<!--var js, fjs = d.getElementsByTagName(s)[0];-->
<!--if (d.getElementById(id)) return;-->
<!--js = d.createElement(s); js.id = id;-->
<!--js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";-->
<!--fjs.parentNode.insertBefore(js, fjs);-->
<!--}(document, 'script', 'facebook-jssdk'));</script>-->
