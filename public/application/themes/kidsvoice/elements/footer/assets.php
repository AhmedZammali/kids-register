<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<script src="https://apis.google.com/js/client.js?onload=function"></script>

<!-- jQuery JS -->
<!--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>-->

<!-- addthis -->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5502fffe31c24b12" async="async"></script>-->

<!-- Bootstrap JS -->
<!--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->

<!-- Owl Carousel JS -->
<!--<script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/owl-carousel/owl.carousel.min.js"></script>-->

<!-- Slick JS -->
<!--<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>-->

<!-- FACEBOOK -->
<!--    <script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/fb.js"></script>-->

<!-- Selectbox JS -->
<!--<script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/selectbox/jquery.selectbox-0.2.js"></script>-->

<!-- ParallaxJs JS -->
<!--<script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/parallaxjs/jquery.parallax.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/parallaxjs/requestAnimationFrame.js"></script>-->

<!-- Fancybox JS -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>-->

<!-- Skrollr JS -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.29/skrollr.min.js"></script>-->

<!-- Fake placeholder -->
<!--<script type="text/javascript" src="--><?php //echo $themePath; ?><!--/js/jquery.placeholder.js"></script>-->

<!-- Vendor JS -->
<script type="text/javascript" src="<?php echo $themePath; ?>/js/vendor.min.js"></script>

<!-- Project JS -->
<script type="text/javascript" src="<?php echo $themePath; ?>/js/kidsvoice.min.js"></script>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5502fffe31c24b12" async="async"></script>
