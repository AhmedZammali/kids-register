<?php

defined('C5_EXECUTE') or die("Access Denied");

?>

<div class="socials">
    <div class="fb-like" data-href="http://www.kidsvoice.ch/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
    <ul>
        <li><a target="_blank" href="https://www.facebook.com/kidsvoicetour?fref=ts" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" href="https://www.youtube.com/user/TourKidsVoice" title="Youtube"><i class="fa fa-youtube"></i></a></li>
    </ul>
</div>
