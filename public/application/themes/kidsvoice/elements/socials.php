<?php defined('C5_EXECUTE') or die("Access Denied"); ?>
<?php $homePage = Page::getByID(1); ?>

<div class="socials">
    <p><?php echo t('Suivez-nous'); ?></p>
	<div class="fb-like" data-href="http://www.kidsvoice.ch/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div><br>
	<div class="clearfix"></div>
    <ul>
        <li><a target="_blank" href="<?php echo $homePage->getAttribute('facebook_link'); ?>" title="<?php echo t('Facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
        <li><a target="_blank" href="<?php echo $homePage->getAttribute('youtube_link'); ?>" title="<?php echo t('Youtube'); ?>"><i class="fa fa-youtube"></i></a></li>
    </ul>
</div>
