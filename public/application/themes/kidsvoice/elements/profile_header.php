<?php defined('C5_EXECUTE') or die("Access Denied"); ?>
<div class="subpage-title row">
    <div class="container texts text-center">
        <h1>
            <?php if ($facebookSession){ ?>
                <?php $facebookUser = VoteManager::getInstance()->getFacebookUser(); ?>
                <strong id="profile"><?php echo t('Salut!'); ?></strong> <?php echo $facebookUser->getName(); ?><br>
                <img src="<?php echo $this->getThemePath(); ?>/img/icons/subpage-profile.png" alt="<?php echo t('Profile'); ?>"><br>
                <button class="btn" type="button" name="fb_logout" value="fb_logout" onclick="fb_logout()"><?php echo t('Déconnexion'); ?></button><br>
                <?php echo t('Les vidéos pour lesquelles j’ai voté'); ?>
            <?php }else{ ?>
                <strong id="profile-not-logged"><?php echo t('Connecte-toi!'); ?></strong>
                <br>
                <button class="btn" type="button" name="fb_login" value="fb_login" onclick="fb_login()"><?php echo t('Connexion'); ?></button>
            <?php } ?>
        </h1>
    </div>
</div>