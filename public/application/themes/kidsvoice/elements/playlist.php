<?php

defined('C5_EXECUTE') or die("Access Denied");

//$container = \DependencyInjection\Container::getInstance();

$language = Localization::activeLanguage();

$videosToPlay = Page::getByID(1)->getAttribute('home_tour_cd_links');
$videosToPlay = explode("\r\n", $videosToPlay);
$formattedVideosToPlay = array();

?>

<?php if ($videosToPlay) { ?>
    <?php foreach ($videosToPlay as $videos) {
        $exploded = explode('||', $videos);
        $formattedVideosToPlay [] = array(
            'video_name' => trim($exploded[0]),
            'video_url' => 'https://www.youtube.com/embed/' . trim($exploded[1] . '?enablejsapi=1'),
        );
    } ?>
<?php }


$nh = Loader::helper('navigation');

$c = Page::getCurrentPage();
$ml = MultilingualSection::getList();
$al = MultilingualSection::getBySectionOfSite($c);

foreach ($ml as $m) {
    if ($m->cID == $al->getCollectionID()) {
        $language = $m->msLanguage;
    }
}

$pl = new PageList();
$pl->filterByPath('/' . $language);
$pl->filterByCollectionTypeHandle('casting');
$pages = $pl->get();

$date_now = date('d.m.Y H:i:s');

foreach ($pages as $page) {
    $tour['place'] = $page->getCollectionName();
    $tour['from_date'] = date('d M Y', strtotime($page->getAttribute('from_date')));
    $tour['to_date'] = $page->getAttribute('to_date');
    $tour['last_remember_time'] = date('Y-m-d H:i:s', strtotime($tour['to_date'] . ' -2 hour'));
    $tour['website'] = $page->getAttribute('website');
    if (date('Y-m-d H:i:s', strtotime($tour['to_date'] . '-4 day')) < date('Y-m-d H:i:s', strtotime($date_now)) &&
        date('Y-m-d H:i:s', strtotime($date_now)) < date('Y-m-d H:i:s', strtotime($tour['to_date'] . '-2 hour'))) {
        $tour['to_date'] = date('d M Y', strtotime($page->getAttribute('to_date')));
        $current_event = $tour;
    } else if (date('Y-m-d H:i:s', strtotime($tour['to_date'] . '-11 day')) < date('Y-m-d H:i:s', strtotime($date_now)) &&
        date('Y-m-d H:i:s', strtotime($date_now)) < date('Y-m-d H:i:s', strtotime($tour['to_date'] . '-4 day'))) {
        $tour['to_date'] = date('d M Y', strtotime($page->getAttribute('to_date')));
        $next_event = $tour;
    }
}

?>

<div class="playlist row">

    <div class="playlist-image">
        <img src="<?php echo $this->getThemePath(); ?>/img/backgrounds/bg-best-grande_mod.jpg" alt="image">
    </div>

    <div class="texts">

        <div class="playlist-text">

            <?php /* $area = new GlobalArea($language . ' Retrouvez les dates '); ?>
        <?php $area->display($c); */ ?>
            <?php if ($current_event): ?>
                <p class="playlist-text-text-big"><?php echo t('Casting en cours'); ?>:</p>
                <p class="playlist-text-text-small"><?php echo $current_event['place'] . ' - ' . $current_event['from_date'] . ' ' . t('to') . ' ' . $current_event['to_date']; ?></p>
                <br>
                <p class="playlist-text-text-big"><?php echo t('Prochain Casting'); ?>:</p>
                <p class="playlist-text-text-small"><?php echo $next_event['place'] . ' - ' . $next_event['from_date'] . ' ' . t('to') . ' ' . $next_event['to_date']; ?></p>
            <?php elseif ($next_event): ?>
            <p class="playlist-text-text-big"><?php echo t('Prochain Casting'); ?>:<br>
            <p class="playlist-text-text-small"><?php echo $next_event['place'] . ' - ' . $next_event['from_date'] . ' ' . t('to') . ' ' . $next_event['to_date']; ?>
            <?php else: ?>
                <p class="playlist-text-text-big"><?php echo t('Bientôt'); ?>
                    <?php endif; ?>
                </p>
                <?php $area = new GlobalArea($language . ' Tous les castings'); ?>
                <?php $area->display($c); ?>

        </div>

    </div>

</div>