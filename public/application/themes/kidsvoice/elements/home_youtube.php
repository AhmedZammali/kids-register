<?php defined('C5_EXECUTE') or die("Access Denied"); ?>

<div class="youtube">
    <button type="button" id="stop-button" class="youtube-close" title="Close">Close</button>
    <iframe src="https://www.youtube.com/embed/0G1xye70o_Y?enablejsapi=1" frameborder="0" allowfullscreen id="video" class="hide"></iframe>
    <div class="bar clearfix">
        <?php $area = new Area('Winner Photo'); ?>
        <?php $area->display($c); ?>
        <div class="title">
            <p><?php echo t('Gagnante Kids Voice Tour 2015 <br/>Alice '); ?></p>
        </div>
        <div class="button-holder">
            <button type="button" id="play-button" class="play-button" title="Play">Play</button>
            <button type="button" id="pause-button" class="pause-button" title="Pause">Pause</button>
            <a href="https://www.youtube.com/embed/0G1xye70o_Y?autplay=1" id="play-fancy-button" class="play-button fancybox" data-fancybox-type="iframe" title="Play">Play</a>
        </div>
        <div class="progress-holder">
			<span class="progressbar">
				<span class="on"></span>
			</span>
        </div>
    </div>
</div>
