<?php defined('C5_EXECUTE') or die("Access Denied"); ?>
<script>

    // global variable for the player
    var player;

    // this function gets called when API is ready to use
    function onYouTubePlayerAPIReady() {
        // create the global player from the specific iframe (#video)
        player = new YT.Player('video', {
            events: {
                // call this function when player is ready to use
                'onReady': onPlayerReady,
        		'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {

        // bind events
        var playButton = document.getElementById("play-button");
        playButton.addEventListener("click", function() {
            player.playVideo();
			player.unMute();
			player.setVolume(100);
        });

        var pauseButton = document.getElementById("pause-button");
        pauseButton.addEventListener("click", function() {
            player.pauseVideo();
        });

        var stopButton = document.getElementById("stop-button");
        stopButton.addEventListener("click", function() {
            player.stopVideo();
        });

        player.addEventListener("onStateChange", updateBar);

        function updateBar () {
			var playerState = player.getPlayerState();
            if (YT.PlayerState.PLAYING) {
                var currentTime = player.getCurrentTime();
                var endTime = player.getDuration();
                var time = (currentTime/endTime)*100;
                $('.inscription .progressbar .on').css('width',time+'%');
                setTimeout(updateBar,200);
            }
        }
	}
		
	var myPlayerState;
	
	function onPlayerStateChange(event) {
		if (event.data == YT.PlayerState.PLAYING && !0) {
			$('#play-button').fadeOut();
			$('#pause-button').fadeIn();
		} else {
			$('#play-button').fadeIn();
			$('#pause-button').fadeOut();
		}
		myPlayerState = event.data;
	}

	if (myPlayerState == 0){
	}

    // Inject YouTube API script
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

</script>