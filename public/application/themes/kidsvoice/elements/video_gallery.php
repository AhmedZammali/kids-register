<?php defined('C5_EXECUTE') or die("Access Denied");
//$container = \DependencyInjection\Container::getInstance();
$language = Localization::activeLanguage(); ?>
<div class="clearfix"></div>
<div class="video-gallery bg-color-white texts padding-vertical-default">
    <div class="container relative">

        <div class="col-xs-12">
            <h2><?php echo t('Galerie Vidéos'); ?></h2>
        </div>

        <div class="relative row <?php if (!$c->isEditMode()){echo 'video-gallery-slider';} ?>">
            <?php $area = new GlobalArea($language . ' Video Gallery'); ?>
            <?php $area->display($c); ?>

<!--			--><?php //$as = new GlobalArea('Video Gallery'); ?>
<!--			--><?php //$as->display($c); ?>
        </div>

        <div class="texts text-center">
            <?php $area = new Area('Videos'); ?>
            <?php $area->display($c); ?>
        </div>
    </div>
</div>
