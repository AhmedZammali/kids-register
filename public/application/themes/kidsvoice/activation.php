<?php 
	defined('C5_EXECUTE') or die(_("Access Denied."));


	$id_event = $_GET['event'];
	$mail = $_GET['mail'];
	$cle = $_GET['cle'];
    $lang = $_GET['lang'];

	$db = \Database::connection();
    $foo = $db->GetAll('SELECT atNumber.avID, atNumber.value FROM atNumber, CalendarEventVersionAttributeValues, CalendarEventVersionOccurrences WHERE atNumber.avID=CalendarEventVersionAttributeValues.avID AND CalendarEventVersionAttributeValues.eventVersionID=CalendarEventVersionOccurrences.eventVersionID AND CalendarEventVersionOccurrences.versionOccurrenceID= ?', [$id_event]);

    $foo2 = $db->GetAll('SELECT actif, id, prenom, region, tranche, casting.date FROM casting WHERE mail = ? AND cle = ?', [$mail, $cle]);
     
		$prenom = $foo2[0]['prenom'];
		$date = $foo2[0]['date']; 
		$region = $foo2[0]['region'];
		$tranche = $foo2[0]['tranche'];

     if ($foo[0]['value'] <= 0) {
		
		$_SESSION["message_retour_valid"] = t('Aucune inscription ouverte pour cette tranche horaire');
		$_SESSION["message_retour_valid1"] = t('Essayez avec une autre tranche');
			$site_url = View::url('/');
            $site_url .= '/'.$lang."#inscri";
		?>
					<script>
						var url_js = <?php echo json_encode($site_url); ?>;
						window.location.replace(url_js) ;
					</script>;
		<?php

     }else{
         if($foo2[0]['actif'] == 0){
 
			$table = 'casting';
			$data = array(
			    'actif' => 1
			);
			$db->update($table, $data, array('id' => $foo2[0]['id']));

     	$new_value = $foo[0]['value']-1;
		$table = 'atNumber';
		$data = array(
		'value' => $new_value,
		);
		$db->update($table, $data, array('avID' => $foo[0]['avID']));

        // envoi 2eme email
		$subject = t("Swiss Voice Tour - Confirmation d'inscription");
			$body = '<p style="font-size : 18px; font-weight: 700;">'.t('Bonjour ').$prenom.'</p>';
			$body .= '<p style="font-size : 16px;">'.t("Suite à ton inscription, nous te transmettons quelques informations pour ta participation à l'édition du Swiss Voice Tour au Centre commercial ").$region.'</p>';
			$body .='<p style="font-size : 16px;">'.t("Merci de prendre note des éléments ci-dessous : ").'</p>';
			$body .='<p style="font-size : 16px; font-weight: 700;">'.t("Ton passage ").'</p>';
			$body .='<p style="font-size : 16px;">'.t("Par ce message, nous avons le plaisir de te confirmer ton jour et tranche horaire de passage sur scène, à savoir : ").'</p>';
			$body .='<p style="font-size : 16px;">'.t("Le ").$date." | ".$tranche.'</p>';
			$body .='<p style="font-size : 16px;">'.t("Pour une question d'organisation, nous te prions de te présenter environ 15 minutes avant le début de ta prestation à l'espace dédié dans le centre commercial et t'annoncer auprès de la personne en charge de la technique.").'</p>';
			$body .='<p style="font-size : 16px; font-weight: 700;">'.t("Ta chanson").'</p>';
			$body .='<p style="font-size : 16px;">'.t("Quant à ta prestation, nous devons avoir une bande instrumentale/PBO (= une version karaoké comme tu peux, par exemple, trouver sur le site http://www.version-karaoke.fr/) sauf si tu viens avec ton propre instrument.").'</p>';

			$body .='<p style="font-size : 16px;">'.t("Tu dois impérativement venir avec ta bande son sur clé USB ou CD.").'</p>';
			$body .='<p style="font-size : 16px; font-weight: 700;">'.t("Et après… ").'</p>';
			$body .='<p style="font-size : 16px;">'.t("Si ta prestation a convaincu le jury, ton parcours continuera le samedi suivant ton passage pour la finale où tu interpréteras une chanson différente du casting. ").'</p>';
			$body .='<p style="font-size : 16px;">'.t("En cas de désistement, nous te prions de bien avoir l'amabilité de nous informer par mail à info@swissvoicetour.ch . ").'</p>';
			$body .='<p style="font-size : 16px;">'.t("Nous te souhaitons plein succès pour ton casting.").'</p>';
			$body .='<hr>';
			$body .='<p style="font-size : 12px;">'.t("Ceci est un courrier généré automatiquement, veuillez ne pas y répondre. ").'</p>';


			$mh = Loader::helper('mail');
			$mh->setSubject($subject);
			$mh->setBodyHTML($body);

			if ($lang=="it"){
			$attachment = \Concrete\Core\File\File::getByID(16);
			$attachment1 = \Concrete\Core\File\File::getByID(15);
			} elseif ($lang=="de"){
			$attachment = \Concrete\Core\File\File::getByID(17);
			$attachment1 = \Concrete\Core\File\File::getByID(14);
			}else {
				$attachment = \Concrete\Core\File\File::getByID(11);
				$attachment1 = \Concrete\Core\File\File::getByID(10);
			}
			
			$mh->addAttachment($attachment);
			$mh->addAttachment($attachment1);
			$mh->to($mail);
			$mh->from('noreply@kidsvoice.ch');
			$mh->sendMail();

			$_SESSION["message_retour_valid"] = t('Votre compte a été activé!');
			$_SESSION["message_retour_valid1"] = t('Merci de reconsultez votre boîte email');
			$site_url = View::url('/');
            $site_url .= '/'.$lang."#inscri";
			?>
						<script>
							var url_js = <?php echo json_encode($site_url); ?>;
							window.location.replace(url_js) ;
						</script>;
			<?php

		}else{
			$_SESSION["message_retour_valid"] = t('Votre compte est déjà activé !');
			$site_url = View::url('/');
            $site_url .= '/'.$lang."#inscri";
			?>
						<script>
							var url_js = <?php echo json_encode($site_url); ?>;
							window.location.replace(url_js) ;
						</script>;
			<?php
		}
	}