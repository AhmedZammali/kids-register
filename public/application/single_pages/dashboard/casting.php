<link rel="stylesheet" href="<?php echo View::url('/'); ?>/application/themes/kidsvoice/css/dash.css" />
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<script language="JavaScript" src="https://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.css">
<?php
$db = \Database::connection();
    $foo = $db->GetAll('SELECT * FROM casting ORDER BY id DESC');
?>          
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID </th>
                            <th>Name</th>
                            <th>First Name</th>
                            <th>Mail</th>
                            <th>Mobile</th>
                            <th>Age</th>
                            <th>Address</th>
                            <th>ZIP code</th>
                            <th>Contry</th>
                            <th>Region</th>
                            <th>Time slot</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>ID </th>
                            <th>Name</th>
                            <th>First Name</th>
                            <th>Mail</th>
                            <th>Mobile</th>
                            <th>Age</th>
                            <th>Address</th>
                            <th>ZIP code</th>
                            <th>Contry</th>
                            <th>Region</th>
                            <th>Time slot</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php foreach ($foo as $key => $value) {  ?>  
                        <tr id="<?=$value['id'];?>">
                            <td><?=$value['id'];?></td>
                            <td contenteditable class='nom'><?=$value['nom'];?></td>
                            <td contenteditable class='prenom'><?=$value['prenom'];?></td>
                            <td contenteditable class='mail'><?=$value['mail'];?></td>
                            <td contenteditable class=mobile><?=$value['mobile'];?></td>
                            <td contenteditable class='age'><?=$value['age'];?></td>
                            <td contenteditable class='adresse'><?=$value['adresse'];?></td>
                            <td contenteditable class='postale'><?=$value['postale'];?></td>
                            <td contenteditable class='ville'><?=$value['ville'];?></td>
                            <td class='region'><?=$value['region'];?></td>
                            <td class='tranche'><?=$value['tranche'];?></td>
                            <td class='date'><?=$value['date'];?></td>
                            <td>

                              <?php if ($value['actif']==1){ ?>

                                <p data-placement="top" data-toggle="tooltip" title="Validate"><button class="btn btn-statuts-valide btn-xs" id="<?=$value['id'];?>" data-title="Validate" data-toggle="modal" style="text-align: center;"><span class="glyphicon glyphicon-ok-circle"></span></button></p>

                              <?php } else { ?>

                                <p data-placement="top" data-toggle="tooltip" title="Validate"><button class="btn btn-statuts-invalide btn-xs" id="<?=$value['id'];?>" data-title="Validate" data-toggle="modal" data-target="#validate" style="text-align: center;"><span class="glyphicon glyphicon-remove-circle"></span></button></p>

                              <?php } ?>

                            </td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Validate"><button class="btn btn-primary btn-xs" id="<?=$value['id'];?>" data-title="Validate" data-toggle="modal" data-target="#edit" style="text-align: center;"><span class="glyphicon glyphicon-check"></span></button></p></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Delete" style="text-align: center;"><button class="btn btn-danger btn-xs" id="<?=$value['id'];?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                        </tr>
                        <?php
                         } ?>
                    </tbody>
                </table>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title custom_align" id="Heading">Edit this entry</h4>
      </div>
          <div class="modal-body">
       
       <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>Voulez vous éditer cet enregistrement?</div>
       
      </div>
        <div class="modal-footer ">
        <button type="button" class="btn btn-success edit" data-dismiss="modal"><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
</div>
    


<div class="modal fade" id="validate" tabindex="-1" role="dialog" aria-labelledby="validate" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title custom_align" id="Heading">Validate this entry</h4>
      </div>
      <div class="modal-body">
         <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>Voulez vous cet modification?</div>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn btn-success validate" data-dismiss="modal"><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
</div>

   
    
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
      </div>
      <div class="modal-body">
         <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span>Voulez vous supprimer cet enregistrement?</div>
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn btn-success remove" data-dismiss="modal"><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
</div>

<script type="text/javascript">
$(document).ready(function() {
     $('#datatable').dataTable( {
       "order": [ 0, 'desc' ]
    });
    $("[data-toggle=tooltip]").tooltip();


} );


$(document).ready(function(){

    $(document).on( "click",".btn.btn-primary.btn-xs" ,function() {
       $( ".btn.btn-primary.btn-xs" ).removeClass( "clicked_btn_edit" );
       $(this).addClass( "clicked_btn_edit" );
    });
    //edit
    $('.btn.btn-success.edit').click(function(){

        var id = $('.clicked_btn_edit').attr('id');
        var nom = $('tr#'+id+' .nom').html();
        var prenom = $('tr#'+id+' .prenom').html();
        var mail = $('tr#'+id+' .mail').html();
        var mobile = $('tr#'+id+' .mobile').html();
        var age = $('tr#'+id+' .age').html();
        var adresse = $('tr#'+id+' .adresse').html();
        var postale = $('tr#'+id+' .postale').html();
        var ville = $('tr#'+id+' .ville').html();
        var region = $('tr#'+id+' .region').html();
        var tranche = $('tr#'+id+' .tranche').html();
        var date = $('tr#'+id+' .date').html();
           // AJAX Request
           $.ajax({
             url: '../../fr/edit',
             type: 'POST',
             data: { id:id,
                     nom : nom,
                     prenom : prenom,
                     mail : mail,
                     mobile : mobile,
                     age : age,
                     adresse : adresse,
                     postale : postale,
                     ville : ville,
                     region : region,
                     tranche : tranche,
                     date : date
              },
             success: function(response){
                console.log(response);
               if(response == 1){
               // Reload data table
            var table = $('#datatable').DataTable();
            location.reload();
              }else{
             alert('Invalid ID.');
              }

            }
           });

    });



    $(document).on( "click",".btn.btn-statuts-invalide.btn-xs" ,function() {
       $( ".btn.btn-statuts-invalide.btn-xs" ).removeClass( "clicked_btn_validate" );
       $(this).addClass( "clicked_btn_validate" );
    });
    //valider
    $('.btn.btn-success.validate').click(function(){
   
        var id = $('.clicked_btn_validate').attr('id');
           // AJAX Request
           $.ajax({
             url: '../../fr/validate',
             type: 'POST',
             data: { id:id
              },
             success: function(response){
                console.log(response);
               if(response == 1){
                $( ".clicked_btn_validate span" ).removeClass( "glyphicon-remove-circle" );
                $( ".clicked_btn_validate span" ).addClass( "glyphicon glyphicon-ok-circle" );
                $( ".clicked_btn_validate span" ).css('color', '#16bf16');
                $( ".clicked_btn_validate" ).removeClass( "btn-statuts-invalide" );
                $( ".clicked_btn_validate" ).addClass( "btn-statuts-invalide" );
                $( ".clicked_btn_validate" ).removeAttr("data-target");
              }else{

                alert('Invalid ID.');
              }

            }
           });

    });



    $(document).on( "click",".btn.btn-danger.btn-xs" ,function() {
       $( ".btn.btn-danger.btn-xs" ).removeClass( "clicked_btn" );
       $(this).addClass( "clicked_btn" );
    });
 // Delete 
 $('.remove').click(function(){
   
   var el = this;
   var id = $('.clicked_btn').attr('id');
   
   // AJAX Request
   $.ajax({
     url: '../../fr/remove',
     type: 'POST',
     data: { id:id },
     success: function(response){
        console.log(response);
       if(response == 1){
     // Remove row from HTML Table
         $('tr#'+id).remove();

      }else{
     alert('Invalid ID.');
      }

    }
   });

 });

});

</script>
